import os
import threading
from sim import *
from connectivity import *
from typing import Optional


def run_sims(method: str, verbose: bool):
  mean_n = 4.0
  num_infected_start = 100
  size = 50000
  dt = 1 # days
  infection_time = 8 # days
  infection_time_sigma = 4 # days
  end_time = 100 # days
  cure_method = CureMethod.GAUSSIAN
  r_values = [1.5, 1.0, 2.0, 2.5]

  dirname = 'output/data_n=%.1f_size=%d/%s' % (mean_n, size, method)
  cd: ConnectivityDistribution = Flat(mean_n)
  if method == 'poisson':
    cd = Poisson(mean_n)
  elif method == 'flat':
    cd = Flat(mean_n)
  elif method == 'geometric':
    cd = Geometric(mean_n)
  elif method == 'gamma05':
    cd = Gamma(mean_n, 0.5)
  elif method == 'gamma1':
    cd = Gamma(mean_n, 1.0)
  elif method == 'gamma2':
    cd = Gamma(mean_n, 2.0)
  elif method == 'gamma4':
    cd = Gamma(mean_n, 4.0)
  elif method == 'gamma8':
    cd = Gamma(mean_n, 8.0)
  else:
    raise Exception('Invalid method: %s' % method)

  if not os.path.exists(dirname):
    os.makedirs(dirname)

  for r_value in r_values:
    infection_rate = r_value / (mean_n * infection_time)
    p = Parameters(
      size=size,
      num_infected_start=num_infected_start,
      connectivity_distribution=cd,
      dt=dt,
      end_time=end_time,
      infection_rate=infection_rate,
      infection_time=infection_time,
      infection_time_sigma=infection_time_sigma,
      cure_method=cure_method)
    sim = Simulation(p=p)
    if not verbose:
      sim.supress_output()
    out = sim.run(prompt=False)

    out.save('%s/run_r=%.2f.npz' % (dirname, r_value))

if __name__ == '__main__':
  if len(sys.argv) == 2:
    run_sims(sys.argv[1], True)
  else:
    for method in ['flat', 'geometric', 'poisson', 'gamma05', 'gamma1', 'gamma2']:
      run_sims(method, True)
