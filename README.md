# Model of Herd Immunity with Symmetric Superspreaders

This project was made on a wim over a few days. It posits that natural variance in individual behavior over populations predicts a lower threshold for herd immunity.

It's based on the idea that so-called "superspreaders" may also be more likely to contract the disease in the first place and thus they are overrepresented in the initial waves of the disease. By taking this into account, it is shown the effective herd immunity threshold is decreased.


## Basic Idea

The basic idea is that we assume that there are individuals within a population who are both more likely to contract covid *and* more likely to spread covid. We assume that these tendencies remain over long periods of time. I will refer to these individuals as having **high connectivity**.

The cause of these tendencies could be many things. The simplest example may be "connectivity". Some individuals make contact with more individuals on a daily basis than others. If we assume this increased contact rate both increases their disease susceptibility and their disease spreading capability, then our assumptions are verified. However there may be other examples that fit these assumptions such as a consistent failure to wash hands or something biological.

The hypothesis is that if populations contain these *highly connected* individuals, then the threshold for herd immunity will decrease. In the initial wave of disease propagation, highly connected individuals will disproportionately become infected. In subsequent waves, their infection rate will diminish. Since these individuals represent a greater contribution to the viruses spreading rate (r0), the effective spreading rate will decrease.

The punchline is that we don't need, say, 70% of the general public to become immune for the disease to die, we only need ~70% of the *highly connected* individuals to become infected. And this happens faster than the overall virus penetration rate.

## Simulation Model

There is a population of `M` individuals. For each individual `i`, we define a "connectivity" `n(i)`. This number represents the number of people individual `i` contacts each day. The simulation runs by iterating over days. There is a parameter $`\alpha`$ which determines the likelihood of transmitting a disease between individuals in contact.

### Infection groups

On each day `t`, for each uninfected individual `i`, we select a set of `G(i,t)` individuals randomly from the population, with selection weighted by connectivity. The size of group `G(i,t)` is given by `n(i)`. Individual `i` then becomes infected with probability proportional to the number of infected individuals in group `G(i,t)`.

Note that `n(i)` influences both the number of people individual `i` contacts each day as well as the likelihood of being selected in another individual's group.

As a first pass, we use the unrealistic model that groups are not symmetric: if individual `j` is in group `G(i,t)`, it does not mean individual `i` is in group `G(j,t)`. Put another way, just because I make contact with you doesn't mean you make contact with me.

### Other assumptions

 - Infected individuals become immune permanently.
 - Infected individuals recover after a random length of time drawn from a normal distribution with mean `T`
 - The connectivity per individual `n(i)` is a constant.
 - The coefficient determining likelihood of disease transmission is a constant (in reality things like temperature and social distancing change this).

## Herd Immunity Threshold

The metric we are measuring is the number of individuals who become infected before the disease dies out. This metric is the herd immunity threshold

## Connectivity distributions

The key variable is the distribution `n(i)`. The idea is to compare the herd immunity threshold for many choices of `n(i)` chosen such that the average `<n(i)>` is a constant. The control group is choosing `n(i)=z` some fixed constant. I also explored Poisson distributions, gamma distributions, and geometric distributions. All of these showed reduced herd immunity thresholds despite having the same base infection rate.

## Brief Results Summary

Here I'll give the numbers for a small subset of data sampled.

Here I set `N=50000` and `T=8` where `T` is the length of infection. The distributions were chosen such that the mean connectivity `M=4`. The infectivity coefficient was chosen such that for an unexposed population and a group size `4`, the viral transmission rate was `r=1.5`.

**Flat Distribution** the flat distribution `n(i)=4` had a peak infection rate of 11.7% and a herd immunity threshold of 59.4%.

![flat result](img/flat_result.png)

**Geometric Distribution** the geometric distribution $`p(n) = (1-1/M)^{n-1}(1/M)`$ had a peak infection rate of 7.4% and a herd immunity threshold of 38.7%.

![geometric result](img/geometric_result.png)

**Gamma Distribution** the gamma distribution with shape `k=0.5` and mean `4` had a peak infection rate of 3.6% and a herd immunity threshold of 20.2%.

![geometric result](img/gamma_result.png)

**Poisson Distribution** the Poisson distribution $`p(n) = (M^n/n!)\exp(-M)`$ had a peak infection rate of 10.2% and a herd immunity threshold of 44.2%.


### Conclusions
The control model, where the number of connections $`n=4`$ is fixed shows a herd immunity threshold of 60% for a background infectivity of $`r_0=1.5`$. On the other hand, the gamma model with shape $`k=0.5`$ shows a herd immunity threshold of just 20.2% with the same background infectivity. This could explain apparent gaps in herd immunity requirements.

The idea that there are individuals who make contact with more people on a daily basis than others strikes me as an obvious truth. This project demonstrates that the presence of these individuals can dramatically change the requirements for herd immunity. The problem with using this as a predictive model is that the exact nature of the distribution of connectivity values is very hard to understand.

Perhaps though this can explain some anomalous data that has appeared as of late such as the suspiciously low spread of the virus in Sweden after its initial epidemic, despite the virus only infecting a small portion of the population.

I imagine this effect is well known in the virology community, but it was fun to think about and model on my own.

## Further work

The main thing I would improve is how infection groups are chosen. Right now they are not symmetric and the algorithm runtime for constructing these groups is $`O(N M \log(N))`$ where `M` is the average connectivity size.

It would be nice to consider a population network graph with random edges selected in each iteration. Though the naive implementation of this has the problem of static connections.

It would also be nice to factor in how the infection cross section changes over time due to social distancing practices. Also we could add simple things like choosing group sizes differently for each day, drawn from a normal distribution whose mean is given by `n(i)`.
